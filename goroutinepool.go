package s3sync

import (
	"context"
	"runtime"
	"sync/atomic"
	"time"
)

type taskRes[T any] struct {
	Value T
	Err   error
}

type asyncPool[T any] struct {
	ctx        context.Context
	taskCh     chan func()
	runningCnt int32
}

func newAsyncPool[T any](ctx context.Context, poolCount int) *asyncPool[T] {
	p := &asyncPool[T]{
		ctx:    ctx,
		taskCh: make(chan func(), 1),
	}

	if poolCount <= 0 {
		poolCount = runtime.NumCPU()
	}

	for i := 0; i < poolCount; i++ {
		go p.loop()
	}

	return p
}

func (t *asyncPool[T]) loop() {
	for {
		select {
		case <-t.ctx.Done():
			return

		case req := <-t.taskCh:
			atomic.AddInt32(&t.runningCnt, 1)
			req()
			atomic.AddInt32(&t.runningCnt, -1)
		}
	}
}

func (t *asyncPool[T]) run(fn func() (T, error)) <-chan taskRes[T] {
	endCh := make(chan taskRes[T], 1)

	t.taskCh <- func() {
		v, err := fn()
		r := taskRes[T]{
			Value: v,
			Err:   err,
		}
		endCh <- r
	}

	return endCh
}

func (t *asyncPool[T]) waitDone() {
	for {
		if atomic.LoadInt32(&t.runningCnt) == 0 {
			return
		}

		time.Sleep(100 * time.Millisecond)
	}
}

package s3sync

import (
	"context"
	"net/url"

	"github.com/aws/aws-sdk-go-v2/service/s3"
	smithyendpoints "github.com/aws/smithy-go/endpoints"
)

var _ s3.EndpointResolverV2 = (*dummyEndpointResolver)(nil)

type dummyEndpointResolver struct {
	endpoint url.URL
}

func NewDummyEndpointResolver(endpoint string) (s3.EndpointResolverV2, error) {
	u, err := url.Parse(endpoint)
	if err != nil {
		return nil, err
	}

	v := &dummyEndpointResolver{
		endpoint: *u,
	}

	return v, nil
}

func (t *dummyEndpointResolver) ResolveEndpoint(ctx context.Context, params s3.EndpointParameters) (smithyendpoints.Endpoint, error) {
	endpoint := t.endpoint

	if params.Bucket != nil {
		endpoint.Path = "/" + *params.Bucket
	}

	v := smithyendpoints.Endpoint{
		URI: endpoint,
	}

	return v, nil
}

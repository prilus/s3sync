package s3sync

import (
	"context"
	"crypto/md5"
	"fmt"
	"io"
	"log"
	"mime"
	"net/url"
	"os"
	"path/filepath"
	"sync"
	"sync/atomic"

	"github.com/aws/aws-sdk-go-v2/aws"
	"github.com/aws/aws-sdk-go-v2/service/s3"
	s3Types "github.com/aws/aws-sdk-go-v2/service/s3/types"
)

var logger = log.New(os.Stdout, "s3sync ", log.LstdFlags|log.Lshortfile)

type syncer struct {
	client  *s3.Client
	verbose bool
}

func NewSyncer(client *s3.Client, verbose bool) *syncer {
	return &syncer{
		client:  client,
		verbose: verbose,
	}
}

func (t *syncer) LocalToS3(localPath string, s3Path string) error {
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()

	asyncPool := newAsyncPool[struct{}](ctx, 0)

	bucket, key, err := parseS3Path(s3Path)
	if err != nil {
		return err
	}

	localMap, err := t.localFileMap(localPath)
	if err != nil {
		return err
	}

	remoteMap, err := t.remoteFileMap(bucket, key)
	if err != nil {
		return err
	}

	if t.verbose {
		logger.Println("localMap", len(localMap), "remoteMap", len(remoteMap))
	}

	lastErr := atomic.Pointer[error]{}

	for k, v := range localMap {
		if remoteMap[k] == v {
			continue
		}

		f, err := os.Open(filepath.Join(localPath, k))
		if err != nil {
			return err
		}

		kk := k
		if key != "" {
			kk = filepath.Join(key, kk)
		}

		asyncPool.run(func() (struct{}, error) {
			mimeType := fileMimeType(kk)

			_, err := t.client.PutObject(ctx, &s3.PutObjectInput{
				Bucket:      aws.String(bucket),
				Key:         aws.String(kk),
				Body:        f,
				ContentType: aws.String(mimeType),
			})

			if err != nil {
				lastErr.CompareAndSwap(nil, &err)
			}

			if t.verbose {
				logger.Println("upload", kk)
			}

			f.Close()

			return struct{}{}, nil
		})
	}

	deleteKeys := []s3Types.ObjectIdentifier(nil)

	for k := range remoteMap {
		if _, exists := localMap[k]; exists {
			continue
		}

		kk := k
		if key != "" {
			kk = filepath.Join(key, kk)
		}

		deleteKeys = append(deleteKeys, s3Types.ObjectIdentifier{
			Key: aws.String(kk),
		})
	}

	for i := 0; i < len(deleteKeys); i += 1000 {
		n := 1000
		if len(deleteKeys)-i < n {
			n = len(deleteKeys) - i
		}

		seperated := deleteKeys[i : i+n]

		asyncPool.run(func() (struct{}, error) {
			_, err := t.client.DeleteObjects(ctx, &s3.DeleteObjectsInput{
				Bucket: aws.String(bucket),
				Delete: &s3Types.Delete{
					Objects: seperated,
				},
			})

			if err != nil {
				lastErr.CompareAndSwap(nil, &err)
			}

			if t.verbose {
				logger.Println("remove count", len(seperated))

				for _, v := range seperated {
					logger.Println("remove", *v.Key)
				}
			}

			return struct{}{}, nil
		})
	}

	asyncPool.waitDone()

	if err := lastErr.Load(); err != nil {
		return *err
	}

	return nil
}

func (t *syncer) remoteFileMap(bucket string, key string) (map[string]string, error) {
	ct := ""
	m := make(map[string]string)

	for {
		req := &s3.ListObjectsV2Input{
			Bucket: aws.String(bucket),
			Prefix: aws.String(key),
		}
		if ct != "" {
			req.ContinuationToken = aws.String(ct)
		}

		res, err := t.client.ListObjectsV2(context.Background(), req)
		if err != nil {
			return nil, err
		}

		for _, v := range res.Contents {
			if v.Key == nil || v.ETag == nil {
				// ?
				continue
			}

			vkey := *v.Key
			if key != "" {
				vkey = vkey[len(key)+1:]
			}

			m[vkey] = *v.ETag
		}

		if res.IsTruncated == nil || !*res.IsTruncated {
			break
		}

		ct = *res.NextContinuationToken

		if t.verbose {
			logger.Println("remotefilemap...", len(m), len(res.Contents))
		}
	}

	return m, nil
}

func (t *syncer) localFileMap(path string) (map[string]string, error) {
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()

	m := make(map[string]string)
	lock := sync.Mutex{}
	asyncPool := newAsyncPool[struct{}](ctx, 0)

	lastErr := atomic.Pointer[error]{}
	err := filepath.Walk(path, func(file string, info os.FileInfo, err error) error {
		if err != nil {
			logger.Println(err)
			return nil
		}

		if info.IsDir() {
			return nil
		}

		asyncPool.run(func() (struct{}, error) {
			etag, err := fileETag(file)
			if err != nil {
				lastErr.CompareAndSwap(nil, &err)
			}

			key := file[len(path)+1:]

			lock.Lock()
			m[key] = etag
			lock.Unlock()

			return struct{}{}, nil
		})

		return nil
	})

	asyncPool.waitDone()

	if err != nil {
		return nil, err
	}
	if err := lastErr.Load(); err != nil {
		return nil, *err
	}

	return m, nil
}

// bucket, key, err
func parseS3Path(s3path string) (string, string, error) {
	u, err := url.Parse(s3path)
	if err != nil {
		return "", "", err
	}

	if u.Scheme != "s3" {
		return "", "", fmt.Errorf("invalid scheme: %s", u.Scheme)
	}

	return u.Host, u.Path, nil
}

func fileETag(path string) (string, error) {
	f, err := os.Open(path)
	if err != nil {
		return "", err
	}
	defer f.Close()

	blk := md5.New()
	if _, err := io.Copy(blk, f); err != nil {
		return "", err
	}

	etag := fmt.Sprintf("\"%x\"", blk.Sum(nil))
	return etag, nil
}

func fileMimeType(filename string) string {
	t, _, err := mime.ParseMediaType(filename)
	if err != nil {
		return "application/octet-stream"
	}

	return t
}
